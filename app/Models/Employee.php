<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $table = 'employees';
    protected $fillable = ['position_id','nama', 'nip', 'tahun_lahir', 'alamat', 'nomor_telepon', 'agama', 'status', 'foto_ktp'];
}
