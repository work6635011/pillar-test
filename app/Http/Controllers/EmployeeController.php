<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $employee = Employee::all();
            return datatables($employee)->addIndexColumn()->toJson();
        }
        return view('employee');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employeeCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'position_id' => 'required|exists:positions,id',
            'nama' => 'required|string',
            'nip' => [
                'required',
                'string',
                Rule::unique('employees')->where(function ($query) {
                    return $query->where('status', true);
                }),
            ],
            'tahun_lahir' => 'required|integer|digits:4',
            'alamat' => 'required|string',
            'nomor_telepon' => 'required|numeric',
            'agama' => 'required|in:Islam,Kristen,Katolik,Hindu,Buddha,Konghucu',
            'status' => 'required|boolean',
            'foto_ktp' => 'nullable|image|mimes:jpg,png|max:2048',
        ]);

        if ($request->hasFile('foto_ktp')) {
            $request->validate([
                'foto_ktp' => 'image|mimes:jpg,png|max:2048',
            ]);

            // Simpan foto ke direktori yang ditentukan
            $imageName = time() . '.' . $request->foto_ktp->extension();
            $request->foto_ktp->move(public_path('images'), $imageName);

            // Tambahkan nama file foto dalam data yang divalidasi
            $validatedData['foto_ktp'] = $imageName;
        }

        try {
            // Buat karyawan baru dengan data yang divalidasi
            Employee::create($validatedData);
            return redirect()->route('employee.index')->with('success', 'Data berhasil disimpan');
        } catch (\Throwable $th) {
            dd($th);
            return redirect()->route('employee.index')->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        return view('employeeEdit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $validatedData = $request->validate([
            'position_id' => 'required|exists:positions,id',
            'nama' => 'required|string',
            'nip' => [
                'required',
                'string',
                Rule::unique('employees')
                    ->ignore($employee->id)
                    ->where(function ($query) {
                        return $query->where('status', true);
                    }),
            ],
            'tahun_lahir' => 'required|integer|digits:4',
            'alamat' => 'required|string',
            'nomor_telepon' => 'required|numeric',
            'agama' => 'required|in:Islam,Kristen,Katolik,Hindu,Buddha,Konghucu',
            'status' => 'required|boolean',
            'foto_ktp' => 'nullable|image|mimes:jpg,png|max:2048',
        ]);

        if ($request->hasFile('foto_ktp')) {
            $request->validate([
                'foto_ktp' => 'image|mimes:jpg,png|max:2048',
            ]);

            // Simpan foto ke direktori yang ditentukan
            $imageName = time() . '.' . $request->foto_ktp->extension();
            $request->foto_ktp->move(public_path('images'), $imageName);

            // Update nama file foto dalam database
            $validatedData['foto_ktp'] = $imageName;
        }

        try {
            $employee->update($validatedData);
            return redirect()->route('employee.index')->with('success', 'Data Berhasil di update');
        } catch (\Throwable $th) {
            return redirect()->route('employee.index')->with('error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        try {
            $employee->delete();
            return redirect()->route('employee.index')->with('success', 'Data Berhasil di hapus');
        } catch (\Throwable $th) {
            return redirect()->route('employee.index')->with('error', $th->getMessage());
        }
    }
}
