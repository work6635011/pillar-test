<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::create([
            'position_id' => 1,
            'nama' => 'John Doe',
            'nip' => '12345',
            'tahun_lahir' => '1990',
            'alamat' => 'Jl. Contoh No. 1',
            'nomor_telepon' => '08123456789',
            'agama' => 'Islam',
            'status' => true,
            'foto_ktp' => '',
        ]);

        Employee::create([
            'position_id' => 2,
            'nama' => 'Jane Doe',
            'nip' => '54321',
            'tahun_lahir' => '1992',
            'alamat' => 'Jl. Contoh No. 2',
            'nomor_telepon' => '08567890123',
            'agama' => 'Kristen',
            'status' => true,
            'foto_ktp' => '',
        ]);

        Employee::create([
            'position_id' => 3,
            'nama' => 'Alice',
            'nip' => '98765',
            'tahun_lahir' => '1988',
            'alamat' => 'Jl. Contoh No. 3',
            'nomor_telepon' => '08765432109',
            'agama' => 'Buddha',
            'status' => true,
            'foto_ktp' => '',
        ]);

        Employee::create([
            'position_id' => 4,
            'nama' => 'Bob',
            'nip' => '56789',
            'tahun_lahir' => '1995',
            'alamat' => 'Jl. Contoh No. 4',
            'nomor_telepon' => '08123456789',
            'agama' => 'Hindu',
            'status' => true,
            'foto_ktp' => '',
        ]);

        Employee::create([
            'position_id' => 5,
            'nama' => 'Eve',
            'nip' => '13579',
            'tahun_lahir' => '1993',
            'alamat' => 'Jl. Contoh No. 5',
            'nomor_telepon' => '08111122233',
            'agama' => 'Konghucu',
            'status' => true,
            'foto_ktp' => '',
        ]);
    }
}
