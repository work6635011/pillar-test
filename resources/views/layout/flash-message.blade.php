<div id="notification">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="btn btn-light" data-bs-dismiss="alert" aria-label="Close">X</button>
        </div>
    @endif

    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="btn btn-light" data-bs-dismiss="alert" aria-label="Close">X</button>
        </div>
    @endif

    @if ($message = Session::get('warning'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="btn btn-light" data-bs-dismiss="alert" aria-label="Close">X</button>
        </div>
    @endif

    @if ($message = Session::get('info'))
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="btn btn-light" data-bs-dismiss="alert" aria-label="Close">X</button>
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Please check the form below for errors</strong>
            @foreach ($errors->all() as $error)
                <span>{{ $error }}</span>
            @endforeach
            <button type="button" class="btn btn-light" data-bs-dismiss="alert" aria-label="Close">X</button>
        </div>
    @endif
</div>

<script>
    // Tampilkan notifikasi saat halaman dimuat
    document.addEventListener('DOMContentLoaded', function() {
        showNotification();
    });

    // Fungsi untuk menampilkan notifikasi
    function showNotification() {
        const notification = document.getElementById('notification');
        notification.classList.remove('d-none');

        // Otomatis tutup setelah 3 detik
        setTimeout(function() {
            closeNotification();
        }, 3000);
    }

    // Fungsi untuk menutup notifikasi
    function closeNotification() {
        const notification = document.getElementById('notification');
        notification.classList.add('d-none');
    }
</script>
