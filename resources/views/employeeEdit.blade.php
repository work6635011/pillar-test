@extends('layout.app')

@section('css')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Jabatan </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Jabatan</li>
        </ol>
    </div>
@endsection
@section('content')
    <div class="card p-5">
        <form action="{{ route('employee.update', $employee->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="nama" class="form-label">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{ $employee->nama }}">
            </div>
            <div class="mb-3">
                <label for="nip" class="form-label">NIP</label>
                <input type="text" class="form-control" id="nip" name="nip" value="{{ $employee->nip }}">
            </div>
            <div class="mb-3">
                @php
                    $data = \App\Models\Position::all();
                @endphp
                <label for="position_id" class="form-group">Position</label>
                <select class="form-control" id="position_id" name="position_id">
                    <option selected disabled>Pilih Posisi</option>
                    @foreach ($data as $item)
                        <option value="{{ $item->id }}" {{ $employee->position_id == $item->id ? 'selected' : '' }}>
                            {{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="tahun_lahir" class="form-group">Tahun Lahir</label>
                <select class="form-control" id="tahun_lahir" name="tahun_lahir">
                    <option selected disabled>Pilih Tahun</option>
                    @for ($tahun = 1990; $tahun <= date('Y'); $tahun++)
                        <option value="{{ $tahun }}" {{ $employee->tahun_lahir == $tahun ? 'selected' : '' }}>
                            {{ $tahun }}</option>
                    @endfor
                </select>
            </div>
            <div class="mb-3">
                <label for="alamat" class="form-label">Alamat</label>
                <textarea class="form-control" id="alamat" name="alamat" rows="3">{{ $employee->alamat }}</textarea>
            </div>
            <div class="mb-3">
                <label for="nomor_telepon" class="form-label">Nomor Telepon</label>
                <input type="text" class="form-control" id="nomor_telepon" name="nomor_telepon"
                    value="{{ $employee->nomor_telepon }}">
            </div>
            <div class="mb-3">
                <label for="agama" class="form-label">Agama</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="agama" id="islam" value="Islam"
                        {{ $employee->agama == 'Islam' ? 'checked' : '' }}>
                    <label class="form-check-label" for="islam">Islam</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="agama" id="kristen" value="Kristen"
                        {{ $employee->agama == 'Kristen' ? 'checked' : '' }}>
                    <label class="form-check-label" for="kristen">Kristen</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="agama" id="Katolik" value="Katolik"
                        {{ $employee->agama == 'Katolik' ? 'checked' : '' }}>
                    <label class="form-check-label" for="Katolik">Katolik</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="agama" id="hindu" value="Hindu"
                        {{ $employee->agama == 'Hindu' ? 'checked' : '' }}>
                    <label class="form-check-label" for="hindu">Hindu</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="agama" id="Buddha" value="Buddha"
                        {{ $employee->agama == 'Buddha' ? 'checked' : '' }}>
                    <label class="form-check-label" for="Buddha">Buddha</label>
                </div>
            </div>
            <div class="mb-3">
                <label for="status" class="form-group">Status</label>
                <select class="form-control" id="status" name="status">
                    <option value="1" {{ $employee->status ? 'selected' : '' }}>Aktif</option>
                    <option value="0" {{ !$employee->status ? 'selected' : '' }}>Tidak Aktif</option>
                </select>

            </div>
            <div class="mb-3">
                <label for="foto_ktp" class="form-label">Foto KTP</label>
                <input class="form-control" type="file" id="foto_ktp" name="foto_ktp"
                    value="{{ $employee->foto_ktp }}" placeholder="Masukkan Foto">{{ $employee->foto_ktp }}</input>
            </div>

            <div class="d-flex justify-content-end pe-0 me-0 text-center">
                <a class="btn btn-danger btn-sm w-auto px-3 me-2 mx-1 d-flex align-items-center" data-bs-toggle="modal"
                    href="#exampleModal" role="button">Hapus</a>
                <a href="{{ route('employee.index') }}" class="btn btn-secondary mx-1">Kembali</a>
                <button type="submit" class="btn btn-primary mx-1">Simpan Perubahan</button>
            </div>


        </form>


        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header tw-justify-between">
                        <h5 class="modal-title" id="exampleModalLabel">Hapus data</h5>
                        <button type="button" class="btn-close btn-sm tw-text-black" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        Apakah anda yakin ingin menghapus data ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm tw-bg-[#6c757d]"
                            data-bs-dismiss="modal">Close</button>
                        <form action="{{ route('employee.destroy', $employee->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm tw-bg-[#dc3545]">Hapus</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
