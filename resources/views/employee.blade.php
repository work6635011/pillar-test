@extends('layout.app')

@section('css')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Jabatan</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Jabatan</li>
        </ol>
    </div>
@endsection
@section('content')
    <div class="card sm mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive p-3">
                <a href="{{ route('employee.create') }}" class="btn btn-primary mb-3">Tambah Data +</a>
                <table class="table align-items-center table-flush" id="dataTable">
                    <thead class="thead-light">
                        <tr>
                            <th>No.</th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Tahun Lahir</th>
                            <th>Alamat</th>
                            <th>No Telp</th>
                            <th>Agama</th>
                            <th>Status</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('employee.index') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        className: "text-center"
                    },
                    {
                        data: 'nip'
                    },
                    {
                        data: 'nama'
                    },
                    {
                        data: 'tahun_lahir'
                    },
                    {
                        data: 'alamat'
                    },
                    {
                        data: 'nomor_telepon'
                    },
                    {
                        data: 'agama'
                    },
                    {
                        data: 'status',
                        render: function(data, type, row) {
                            return data == '1' ?
                                '<div class="bg-success rounded text-center text-white"> Aktif </div>' :
                                '<div class="bg-danger rounded text-center text-white"> Tidak Aktif </div>';
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, row) {
                            var editUrl = "{{ route('employee.edit', ':id') }}";
                            editUrl = editUrl.replace(':id', row.id);
                            return '<a href="' + editUrl +
                                '" class="bg-primary rounded px-2 text-white d-flex m-1"> Edit <i class="pl-2 fs-5 bi bi-pencil-square"></i></a>';
                        }
                    }

                ]
            });
        });
    </script>
@endpush
