@extends('layout.app')

@section('css')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Jabatan</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Jabatan</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="card p-5">
        <form action="{{ route('employee.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="nama" class="form-label">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}">
            </div>
            <div class="mb-3">
                <label for="nip" class="form-label">NIP</label>
                <input type="text" class="form-control" id="nip" name="nip" value="{{ old('nip') }}">
            </div>
            <div class="mb-3">
                @php
                    $data = \App\Models\Position::all();
                @endphp
                <label for="position_id" class="form-group">Position</label>
                <select class="form-control" id="position_id" name="position_id">
                    <option selected disabled>Pilih Posisi</option>
                    @foreach ($data as $item)
                        <option value="{{ $item->id }}" >{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="tahun_lahir" class="form-group">Tahun Lahir</label>
                <select class="form-control" id="tahun_lahir" name="tahun_lahir">
                    <option selected disabled>Pilih Tahun</option>
                    @for ($tahun = 1990; $tahun <= date('Y'); $tahun++)
                        <option value="{{ $tahun }}" {{ old('tahun_lahir') == $tahun ? 'selected' : '' }}>
                            {{ $tahun }}</option>
                    @endfor
                </select>
            </div>
            <div class="mb-3">
                <label for="alamat" class="form-label">Alamat</label>
                <textarea class="form-control" id="alamat" name="alamat" rows="3">{{ old('alamat') }}</textarea>
            </div>
            <div class="mb-3">
                <label for="nomor_telepon" class="form-label">Nomor Telepon</label>
                <input type="text" class="form-control" id="nomor_telepon" name="nomor_telepon"
                    value="{{ old('nomor_telepon') }}">
            </div>
            <div class="mb-3">
                <label for="agama" class="form-label">Agama</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="agama" id="islam" value="Islam"
                        {{ old('agama') == 'Islam' ? 'checked' : '' }}>
                    <label class="form-check-label" for="islam">Islam</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="agama" id="Kristen" value="Kristen"
                        {{ old('agama') == 'Kristen' ? 'checked' : '' }}>
                    <label class="form-check-label" for="Kristen">Kristen</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="agama" id="Buddha" value="Buddha"
                        {{ old('agama') == 'Buddha' ? 'checked' : '' }}>
                    <label class="form-check-label" for="Buddha">Buddha</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="agama" id="Hindu" value="Hindu"
                        {{ old('agama') == 'Hindu' ? 'checked' : '' }}>
                    <label class="form-check-label" for="Hindu">Hindu</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="agama" id="Konghucu" value="Konghucu"
                        {{ old('agama') == 'Konghucu' ? 'checked' : '' }}>
                    <label class="form-check-label" for="Konghucu">Konghucu</label>
                </div>
                <!-- Tambahkan pilihan agama lainnya sesuai kebutuhan -->
            </div>
            <div class="mb-3">
                <label for="status" class="form-group">Status</label>
                <select class="form-control" id="status" name="status">
                    <option value="1" {{ old('status') == '1' ? 'selected' : '' }}>Aktif</option>
                    <option value="0" {{ old('status') == '0' ? 'selected' : '' }}>Tidak Aktif</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="foto_ktp" class="form-label">Foto KTP</label>
                <input class="form-control" type="file" id="foto_ktp" name="foto_ktp"
                    value="{{ old('foto_ktp') }}" placeholder="Masukkan Foto">
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
@endsection
